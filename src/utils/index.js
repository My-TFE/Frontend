import {isEmpty} from 'lodash'

export function validateEmail(email) {
	const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}
export function validateName(name) {
	const reg =/(^[a-zA-Z]{1}[a-zA-Z0-9]{2,11})$/
	return reg.test(name)
}
export function validationPassword(password) {
	const reg = /((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))/
	return reg.test(password) &&  password.length > 6 && password.length < 11
}

export function validationPhoneNumber(phoneNumber) {
	const reg = /^(04)(55|56|65|66|67|68|60|70|79|83|89|90|99)(\d{2}){3}$/
	return reg.test(phoneNumber)
}
export function isNumber(value) {
	const reg = /^(([1-9]\d{1,2})|0)$/
	return reg.test(value)
}

export function validateTitle(title) {
  const reg = /^(.{3,30})$/
	return reg.test(title)
}

export function validateDesc(desc) {
  const reg = /^.*$/
  return reg.test(desc)
}

export function validateWeight(value) {
	const reg = /(^(([0-9])|([1-9][0-9])|(1[0-4][0-9])|150))$/
	return reg.test(value)
}
export function validateNbrRepetition(value) {
	const reg = /(^(([1-9])|([1-9][0-9])|(1[0-4][0-9])|150))$/
	return reg.test(value)
}

export const Utils = Object.freeze({
	Object: {
		valuesAreEmpty: function(object) {
			return Object.keys(object).every(key => isEmpty(object[key]))
		},
		allValuesAreFilled: function(object) {
			return Object.keys(object).every(key => object[key])
		}
	}
})