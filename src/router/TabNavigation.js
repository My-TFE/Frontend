import React from 'react'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation'
import Customers from '../components/customers'
import Settings from '../components/Settings'
import Customer from '../components/Customer'
import Profile from '../components/Profile'
import Exercises from '../components/Exercises'
import Session from '../components/Session'
import ExerciseDetails from '../components/ExerciseDetails'
import BodyMeasuresDetails from '../components/BodyMeasuresDetails'


const CustomersTab = createStackNavigator({
  Customers: {
    screen: Customers,
    navigationOptions: () => ({
      title: 'Customers'
    })
  },
  Customer: {
    screen: Customer,
  },
  Profile: {
    screen: Profile,
    navigationOptions: () => ({
      title: 'Profile'
    })
  },
  Session: {
    screen: Session,
    navigationOptions: () => ({
      title: 'Sessions'
    })
  },
  Exercises: {
    screen: Exercises,
    navigationOptions: () => ({
      title: 'Exercises'
    })
  },
  ExerciseDetails: {
    screen: ExerciseDetails,
    navigationOptions: () => ({
      title: 'details'
    })
  },
  BodyMeasuresDetails: {
    screen: BodyMeasuresDetails,
    navigationOptions: () => ({
      title: 'Body Measures Details'
    })
  }

})

export default createBottomTabNavigator({
  Customers: {
    path: '/customers',
    screen: CustomersTab,
    navigationOptions: {
      tabBarLabel: 'Customers',
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcons
          name='people-outline'
          size={20}
          style={{ color: tintColor }}
        />
      ),
      tabBarOptions: {
        activeTintColor: 'white',
        labelStyle: {
          fontSize: 15,
        },
        style: {
          backgroundColor: '#3382F9',
        },
      }
    }
  },
  Settings: {
    path: '/settings',
    screen: Settings,
    navigationOptions: {
      tabBarLabel: 'Settings',
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcons
          name='settings'
          size={20}
          style={{ color: tintColor }}
        />
      ),
      tabBarOptions: {
        activeTintColor: 'white',
        labelStyle: {
          fontSize: 15,
        },
        style: {
          backgroundColor: '#3382F9',
        },
      }
    }
  }
})
