
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon as ElementIcon, ListItem, } from 'react-native-elements'
import { StyleSheet, Modal, TextInput, TouchableHighlight, Image } from 'react-native'
import { Container, Button, Text, View, Header, Spinner, Content, Form, Label, Input, Item, Icon, Left, Right, Body, Title, Toast } from 'native-base'
import currentUserActions from '../store/currentUser/actions'
import * as utils from '../utils/index'
import Errors from './errors'

@connect(
  state => {
    return ({
      currentUser: state.currentUser
    })
  },
  dispatch => bindActionCreators({ ...currentUserActions }, dispatch)
)

class Login extends React.Component {

  componentWillMount() {
    this.setState({
      modalVisible: false,
      credentials: {
        email: 'noreddine@gmail.com',
        password: 'coutcout1990'
      },
      personData: {
        firstName: '',
        lastName: '',
        email: '',
        password: '',

      },
      errors: [],
      errorsRegister: []
    })
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.currentUser.actions.signInWithEmail.success && nextProps.currentUser.actions.signInWithEmail.success) {
      this.props.getSignedInUser()
    }
    if (!this.props.currentUser.actions.signInWithEmail.error && nextProps.currentUser.actions.signInWithEmail.error) {
      this._signInwithEmailError()
    }
    if (!this.props.currentUser.actions.signup.success && nextProps.currentUser.actions.signup.success) {
      this._setModalVisible(!this.state.modalVisible)
      this.props.signInWithEmail({ email: this.state.personData.email, password: this.state.personData.password })
    }
  }
  _setModalVisible(value) {
    this.setState({ modalVisible: value })
  }

  _signInwithEmailError() {
    this.setState(prevState => ({
      errors: [...prevState.errors, 'email or password is not valid !'],
      credentials: { email: '', password: '' }
    }))
  }

  _validateFormRegister() {
    let res = true
    if (!utils.validateEmail(this.state.personData.email)) {
      this.setState(prevState => ({
        errorsRegister: [...prevState.errorsRegister, 'this email is not valid']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.firstName)) {
      this.setState(prevState => ({
        errorsRegister: [...prevState.errorsRegister, 'the firstName is not valid, eg: radi ']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.lastName)) {
      this.setState(prevState => ({
        errorsRegister: [...prevState.errorsRegister, 'the lastName is not valid, eg: noreddine']
      }))
      res = false
    }
    if (!utils.validationPassword(this.state.personData.password)) {
      this.setState(prevState => ({
        errorsRegister: [...prevState.errorsRegister, 'the password must contain : min 6 characters, max 10 characters and  numbers, tiny, capital letter ']
      }))
      res = false
    }
    return res
  }
  _signup() {
    this.setState({ errorsRegister: [] })
    this._validateFormRegister() ? this.props.signup(this.state.personData) : null
  }
  _signInWithEmail() {
    this.setState({ errors: [] })
    this.props.signInWithEmail(this.state.credentials)
  }

  render() {
    return (
      <Container style={{ flex: 1 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={require('../images/fitness1.jpg')}
            style={{ width: 130, height: 130, borderRadius: 100, borderWidth: 2, borderColor: 'black' }}
          />
          <Text style={{ justifyContent: 'flex-end', fontSize: 25, marginTop: 40 }}>Welcome !</Text>
        </View>
        <Content padder scrollEnabled={false} contentContainerStyle={styles.content} style={{ flex: 1 }}>
          <Form>
            <Item floatingLabel>
              <Icon active name='mail' />
              <Label>Email</Label>
              <Input value={this.state.credentials.email}
                keyboardType='email-address'
                onChangeText={(email) => { this.setState({ credentials: { ...this.state.credentials, email } }), this.setState({ errors: [] }) }}
              />
            </Item>
            <Item floatingLabel>
              <Icon active name='lock' />
              <Label>Password</Label>
              <Input value={this.state.credentials.password} secureTextEntry onChangeText={(password) => { this.setState({ credentials: { ...this.state.credentials, password } }), this.setState({ errors: [] }) }} />
            </Item>
          </Form>
            <Errors errors={this.state.errors} />
          <Button full style={styles.button} onPress={() => this._signInWithEmail()}><Text>Sign in</Text></Button>
          <View style={styles.text}>
            <Text>Don't have an account yet?</Text>
            <Text onPress={() => { this._setModalVisible(!this.state.modalVisible), this.setState({ errors: [] }) }} style={styles.signup}>Sign up</Text>
          </View>
        </Content>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {this._setModalVisible(!this.state.modalVisible), this.setState({ errorsRegister: [] })}}
        >
          <ListItem
            title='Register'
            containerStyle={styles.listItemContainer}
            titleStyle={styles.titleListItemModal}
            leftIcon={<ElementIcon name='chevron-left' onPress={() => { this._setModalVisible(!this.state.modalVisible), this.setState({ errorsRegister: [] }) }} />}
            hideChevron
          />
          <Container>
            <Form>
              <Item inlineLabel>
                <Label>First Name</Label>
                <Input value={this.state.personData.firstName}
                  onChangeText={(firstName) => this.setState({ personData: { ...this.state.personData, firstName } })} />
              </Item>
              <Item inlineLabel>
                <Label>Last Name</Label>
                <Input value={this.state.personData.lastName}
                  onChangeText={(lastName) => this.setState({ personData: { ...this.state.personData, lastName } })} />
              </Item>
              <Item inlineLabel>
                <Label>Email</Label>
                <Input value={this.state.personData.email}
                  onChangeText={(email) => this.setState({ personData: { ...this.state.personData, email } })} />
              </Item>
              <Item inlineLabel>
                <Label>Password</Label>
                <Input value={this.state.personData.password}
                  secureTextEntry
                  onChangeText={(password) => this.setState({ personData: { ...this.state.personData, password } })} />
              </Item>
              <Button full style={{ marginTop: 60, margin: 10 }} onPress={() => { this._signup() }}><Text>Register</Text></Button>
            </Form>
            <Errors errors={this.state.errorsRegister} />
          </Container>
        </Modal>
      </Container>
    )
  }
}


const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
    paddingBottom: 20
  },
  button: {
    marginTop: 40
  },
  text: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20
  },
  signup: {
    marginLeft: 10,
    color: 'blue',
  },
  mark: {
    width: '50%',
    alignSelf: 'center'
  },
  modalHeader: {
    backgroundColor: 'white'
  },
  loader: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorMessage: {
    paddingLeft: 15,
    paddingTop: 5,
    color: 'red'
  },
  titleListItemModal: {
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemContainer: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2
  }
})


export default Login


