import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon as ElementIcon, ListItem, Button, } from 'react-native-elements'
import moment from 'moment'
import { StyleSheet, ScrollView, FlatList, Modal, DatePickerAndroid, Keyboard } from 'react-native'
import { Container, Content, Body, Text, Left, List, Right, Thumbnail, Fab, Footer, View, Header, Title, Form, Label, Input, Item, Separator, InputGroup } from 'native-base'
import * as exercisesActions from '../store/exercises/actions'
import { greenAdd, road } from '../colors/colors'
import * as utils from '../utils/index'
import Errors from './errors'

@connect(
  state => {
    return ({
      exercises: state.exercises
    })
  },
  dispatch => bindActionCreators({ ...exercisesActions }, dispatch)
)

class Exercises extends React.Component {

  componentWillMount() {
    const sessionId = this.props.navigation.state.params.session._id
    this.setState({
      modalVisible: false,
      deleteExerciseModalVisible: false,
      updateExerciseModalVisible: false,
      exerciseDelete: '',
      exerciseData: {
        title: '',
        description: '',
        recoveryTime: '',
        session: sessionId
      },
      errors: []
    })
    this.props.getExercises(sessionId)

  }

  componentWillReceiveProps(nextProps) {

    if (!this.props.exercises.actions.createExercise.success && nextProps.exercises.actions.createExercise.success) {
      this._setModalVisible(false)
      this.props.getExercises(this.props.navigation.state.params.session._id)
    }
    if (!this.props.exercises.actions.deleteExercise.success && nextProps.exercises.actions.deleteExercise.success) {
      this._seDeleteExerciseModalVisible(false)
      this.props.getExercises(this.props.navigation.state.params.session._id)
    }
    if (!this.props.exercises.actions.updateExercise.success && nextProps.exercises.actions.updateExercise.success) {
      this.props.getExercises(this.props.navigation.state.params.session._id)
      this.setState({ updateExerciseModalVisible: false })
    }

  }
  _setModalVisible(value) {
    this.setState({ modalVisible: value })
  }
  _seDeleteExerciseModalVisible(value) {
    this.setState({ deleteExerciseModalVisible: value })
  }
  _deleteExercise() {
    this.props.deleteExercise(this.state.exerciseDelete._id)
    this.setState({ exerciseDelete: '' })
  }
  _setUpdateExerciseModalVisible(value) {
    this.setState({ updateExerciseModalVisible: value })
  }
  _setExercise(exercise) {
    this.setState(prevState => ({
      exerciseData: {
        ...prevState.exerciseData,
        _id: `${exercise._id}`,
        title: `${exercise.title}`,
        description: `${exercise.description}`,
        recoveryTime: `${exercise.recoveryTime}`
      }
    }))
  }

  _validateForm() {
    let res = true
    if (!utils.validateTitle(this.state.exerciseData.title)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the title is not valid !, eg: squat']
      }))
      res = false
    }
    if (!utils.validateDesc(this.state.exerciseData.description)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the description must be less than 250 charcters ! ']
      }))
      res = false
    }
    if (!utils.isNumber(this.state.exerciseData.recoveryTime)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the recoveryTime is not valid !, eg: 45 ']
      }))
      res = false
    }
    return res
  }
  _createExercise() {
    this.setState({errors: []})
    return this._validateForm() ? this.props.createExercise(this.state.exerciseData) : null
  }
  _updateExercise() {
    this.setState({errors: []})
    return this._validateForm() ? this.props.updateExercise(this.state.exerciseData) : null
  }


  render() {
    const { exercises, createExercise } = this.props
    const session = this.props.navigation.state.params.session
    return (
      <Container style={{ flex: 1 }}>
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', paddingTop: 10, justifyContent: 'space-between', backgroundColor: '#51ADFF' }}>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text style={{ color: road, fontSize: 20 }}>{session.title}</Text>
            <Text style={{ fontStyle: 'italic', color: road }}>{moment(session.createdAt).format('DD MMM YY', 'fr')}</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <ElementIcon type='material-community' name='dumbbell' size={50} />
          </View>
          <View style={{ flex: 0.5, flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'flex-end', padding: 5 }}>
            <View ><Text style={{ color: road }}>{exercises.data.length} Exercise</Text></View>
            <View ><Text style={{ color: road }}>{exercises.data.reduce((res, exercises) => res + exercises.sets.length, 0)} Sets</Text></View>
            <View ><Text style={{ color: road }}>{exercises.data.reduce((res, exercises) => res + exercises.sets.reduce((res, set) => res + set['nbrRepetition'], 0), 0)} Reps</Text></View>
          </View>
        </View>
        <View style={{ flex: 2 }}>
        {(exercises.success && exercises.data.length === 0) && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin: 15, flexDirection:'column' }}>
            <Text>You don't have exercise yet !</Text>
            <Text>Please create new exercises </Text>
          </View>}
          <ScrollView>
            <List style={{}}>
              {
                exercises.success && exercises.data.map((exercise, i) => {
                  return (
                    <View key={i} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', margin: 10, borderBottomWidth: 1, padding: 5, borderBottomColor: '#E0E5E2' }}>
                      <View style={{ flex: 2, flexDirection: 'column' }}>
                        <Text>{exercise.title}</Text>
                        <Text note>{exercise.sets.length} sets</Text>
                      </View>
                      <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <ElementIcon type='material-community' name='pencil' color='#10ADFF' onPress={() => { this._setUpdateExerciseModalVisible(!this.state.updateExerciseModalVisible), this._setExercise(exercise) }} />
                        <ElementIcon type='material-community' name='delete' color='red' onPress={() => { this._seDeleteExerciseModalVisible(!this.state.deleteExerciseModalVisible), this.setState({ exerciseDelete: exercise }) }} />
                      </View>
                      <View style={{ flex: 1.65, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text note>{moment(exercise.createdAt).format('DD/MM/YYYY')}</Text>
                        <ElementIcon type='material-community' name='chevron-right' color='green' onPress={() => this.props.navigation.navigate('ExerciseDetails', { exercise })} />
                      </View>
                    </View>
                  )
                })
              }
            </List>
          </ScrollView>
        </View>
        <Fab position='bottomRight' style={{ backgroundColor: greenAdd }} containerStyle={{ right: 10, bottom: 10 }} onPress={() => this._setModalVisible(!this.state.modalVisible)}>
          <ElementIcon name='add' color='white' />
        </Fab>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this._setModalVisible(!this.state.modalVisible)}
        >
          <Container>
            <ListItem
              title='New Exercise'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setModalVisible(!this.state.modalVisible), this.setState({errors: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._createExercise()} />}
            />
            <Form>
              <Item inlineLabel>
                <Label>Title</Label>
                <Input onChangeText={(title) => this.setState({ exerciseData: { ...this.state.exerciseData, title } })} />
              </Item>
              <Item inlineLabel>
                <Label>Description</Label>
                <Input onChangeText={(description) => this.setState({ exerciseData: { ...this.state.exerciseData, description } })} />
              </Item>
              <Item inlineLabel>
                <Label>Recovery Time</Label>
                <Input onChangeText={(recoveryTime) => this.setState({ exerciseData: { ...this.state.exerciseData, recoveryTime } })} />
                <Text note style={{ marginRight: 5 }} > second </Text>
              </Item>
            </Form>
           <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.deleteExerciseModalVisible}
          onRequestClose={() => this._seDeleteExerciseModalVisible(!this.state.deleteExerciseModalVisible)}
        >
          <Container>
            <ListItem
              title='Delete Exercise'
              titleStyle={{ alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20, }}
              hideChevron
            />
            <View style={{ margin: 10, alignItems: 'center' }}>
              <Text>Do you really want to remove</Text>
              <Text> The Exercise ?</Text>
            </View>
            <View style={{ margin: 20, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
              <Button title='Cancel' buttonStyle={{ backgroundColor: "#66FCB1", width: 80, borderRadius: 3 }} onPress={() => this._seDeleteExerciseModalVisible(!this.state.deleteExerciseModalVisible)} />
              <Button title='Delete' buttonStyle={{ backgroundColor: "#FC4E7D", width: 80, borderRadius: 3 }} onPress={() => { this._deleteExercise() }} />
            </View>
          </Container>
        </Modal>
        <Modal
          animationType={'none'}
          transparent={false}
          visible={this.state.updateExerciseModalVisible}
          onRequestClose={() => this._setUpdateExerciseModalVisible(!this.state.updateExerciseModalVisible)}
        >
          <Container>
            <ListItem
              title='Update Exercise'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setUpdateExerciseModalVisible(!this.state.updateExerciseModalVisible), this.setState({errors: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._updateExercise()} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>Title</Label>
                <Input value={this.state.exerciseData.title} onChangeText={(title) => this.setState({ exerciseData: { ...this.state.exerciseData, title } })} />
              </Item>
              <Item floatingLabel>
                <Label>Description</Label>
                <Input value={this.state.exerciseData.description === '' ? this.state.exerciseData.description : ''} onChangeText={(description) => this.setState({ exerciseData: { ...this.state.exerciseData, description } })} />
              </Item>
              <Item floatingLabel>
                <Label>Recovery Time</Label>
                <Input value={this.state.exerciseData.recoveryTime} onChangeText={(recoveryTime) => this.setState({ exerciseData: { ...this.state.exerciseData, recoveryTime } })} />
              </Item>
            </Form>
            <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
      </Container>
    )
  }
}


const styles = StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
  },
  ListeCustomers: {
    flex: 1
  },
  header: {
    height: 1,
    backgroundColor: 'gray'
  },
  titleListItemModal: {
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemContainer: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2
  }
})
export default Exercises