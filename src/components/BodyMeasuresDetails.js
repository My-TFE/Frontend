import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NavigationActions } from 'react-navigation'
import moment from 'moment'
import { StyleSheet, View, TextInput, Modal, Keyboard } from 'react-native'
import { Icon as ElementIcon, ListItem } from 'react-native-elements'
import { Container, Text, Header, List, Spinner, Content, Form, Label, Input, Item, Icon, Left, Right, Body, Title, Toast } from 'native-base'
import { ScrollView } from '../../node_modules/react-native-gesture-handler'
import * as customersActions from '../store/customers/actions'
import * as utils from '../utils/index'

@connect(
  state => {
    return ({
      customers: state.customers
    })
  },
  dispatch => bindActionCreators({ ...customersActions }, dispatch)
)

export default class BodyMeasuresDetails extends Component {

  componentWillMount() {
    const measures = this.props.navigation.state.params.measure 
    this.setState({
      bodyMeasureData: {
        _id: measures._id,
        height: measures.height,
        weight: measures.weight,
        shoulder: measures.shoulder,
        pectorals: measures.pectorals,
        arm: measures.arm,
        belly: measures.belly,
        hips: measures.hips,
        thigh: measures.thigh,
        calf: measures.calf,
        measuredAt: measures.measuredAt
      },
      errors: []
    })
  }
  _updateCustomer() {
    const measures = this.props.navigation.state.params.measure
    const customer = this.props.navigation.state.params.customer
    const index = customer.bodyMeasure.findIndex(el => el._id == measures._id)
    customer.bodyMeasure.splice(index, 1, this.state.bodyMeasureData)
    this.props.updateCustomer(customer)
    this.props.navigation.goBack()
  }
  _updateBodyMeasureCustomer() {
    this.setState({ errors: [] })
    return this._validateBodyForm() ? this._updateCustomer() : null
  }

  _validateBodyForm() {
    let res = true
    if (this._bodyValueEmpty()) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'impossible to register the body measurement, because the all values are empty']
      }))
      res = false
    }
    if (!this._bodyValueValid()) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'one or more value is not valid']
      }))
      res = false
    }
    return res
  }

  _bodyValueEmpty() {
    const bodyMeasure = this.state.bodyMeasureData
    return (bodyMeasure.height == '' && bodyMeasure.weight == '' &&
      bodyMeasure.shoulder == '' && bodyMeasure.pectorals == '' &&
      bodyMeasure.arm == '' && bodyMeasure.belly == '' &&
      bodyMeasure.hips == '' & bodyMeasure.thigh == '' && bodyMeasure.calf == '')
  }
  _bodyValueValid() {
    const bodyMeasure = this.state.bodyMeasureData
    return (utils.isNumber(bodyMeasure.height) && utils.isNumber(bodyMeasure.weight) && utils.isNumber(bodyMeasure.shoulder) &&
      utils.isNumber(bodyMeasure.pectorals) && utils.isNumber(bodyMeasure.arm) && utils.isNumber(bodyMeasure.belly) &&
      utils.isNumber(bodyMeasure.hips) && utils.isNumber(bodyMeasure.thigh) && utils.isNumber(bodyMeasure.calf))
  }
  render() {
    return (
      <Container>
        <ListItem
          title={<View style={{ flexDirection: 'row' }}><Text style={{ marginRight: 5, fontStyle: 'italic' }}>Measures of</Text><Text style={{ fontStyle: 'italic' }}>{moment(this.state.bodyMeasureData.measuredAt).format('DD/MM/YYYY')}</Text></View>}
          rightIcon={<ElementIcon name='save' onPress={() => { this._updateBodyMeasureCustomer() }} size={30} />}
        />
        <ScrollView>
          <Form>
            <Item inlineLabel>
              <Label>Height</Label>
              <Input value={`${this.state.bodyMeasureData.height}`}
                onChangeText={(height) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, height } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Weight</Label>
              <Input value={`${this.state.bodyMeasureData.weight}`}
                onChangeText={(weight) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, weight } })} />
              <Text note style={{ marginRight: 5 }} > kg </Text>
            </Item>
            <Item inlineLabel>
              <Label>Shoulder</Label>
              <Input value={`${this.state.bodyMeasureData.shoulder}`}
                onChangeText={(shoulder) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, shoulder } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Pectorals</Label>
              <Input value={`${this.state.bodyMeasureData.pectorals}`}
                onChangeText={(pectorals) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, pectorals } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Arm</Label>
              <Input value={`${this.state.bodyMeasureData.arm}`}
                onChangeText={(arm) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, arm } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Belly</Label>
              <Input value={`${this.state.bodyMeasureData.belly}`}
                onChangeText={(belly) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, belly } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Hips</Label>
              <Input value={`${this.state.bodyMeasureData.hips}`}
                onChangeText={(hips) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, hips } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Thigh</Label>
              <Input value={`${this.state.bodyMeasureData.thigh}`}
                onChangeText={(thigh) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, thigh } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
            <Item inlineLabel>
              <Label>Calf</Label>
              <Input value={`${this.state.bodyMeasureData.calf}`}
                onChangeText={(calf) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, calf } })} />
              <Text note style={{ marginRight: 5 }} > cm </Text>
            </Item>
          </Form>
          <View style={{ flexDirection: 'column', justifyContent: 'center', margin: 10 }}>
            {this.state.errors.length > 0 && this.state.errors.map((element, i) => {
              return (
                <Text key={i} note style={{ color: 'red', marginBottom: 5 }}>* {element}</Text>
              )
            })}
          </View>
        </ScrollView>
      </Container>
    )
  }
}