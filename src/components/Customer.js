import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Icon, ListItem } from 'react-native-elements'
import { Container, Text, Content, Body, Header, Right, List, Left } from 'native-base'
class Customer extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: `${navigation.state.params.customer.firstName} ${navigation.state.params.customer.lastName}`
  })
  render() {
    const customer = this.props.navigation.state.params.customer
    return (
      <Content contentContainerStyle={styles.content}>
        <Header style={styles.header} />
        <List>
          <ListItem 
          title= 'Programs'
          titleStyle= {styles.titleListItem}
          leftIcon={<Icon name='list' color='red' />}
          onPress={() =>  this.props.navigation.navigate('Session', { customerId: customer._id })}
          chevronColor='green'
          />
          <ListItem 
          title= 'Profile'
          titleStyle= {styles.titleListItem}
          leftIcon={<Icon name='person' color='#4287EF'  />}
          onPress={() => this.props.navigation.navigate('Profile', {customer: customer})}
          chevronColor='green'
          />
        </List>
      </Content>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    height: 1,
    backgroundColor: 'gray'
  },
  titleListItem: {
    marginLeft: 10,
    fontWeight: 'normal',
    color: 'black',
    
  }
})


export default Customer