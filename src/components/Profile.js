import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { StyleSheet, View, TextInput, Modal, Keyboard } from 'react-native'
import { Icon as ElementIcon, ListItem } from 'react-native-elements'
import { Container, Text, Header, List, Spinner, Content, Form, Label, Input, Item, Fab, Icon, Left, Right, Body, Title, Toast } from 'native-base'
import moment from 'moment'
import DatePicker from 'react-native-datepicker'
import PureChart from 'react-native-pure-chart'
import { ScrollView } from '../../node_modules/react-native-gesture-handler';
import * as customersActions from '../store/customers/actions'
import { greenAdd } from '../colors/colors'
import * as utils from '../utils/index'
import Errors from './errors'

@connect(
  state => {
    return ({
      customers: state.customers
    })
  },
  dispatch => bindActionCreators({ ...customersActions }, dispatch)
)


class Profile extends React.Component {

  componentWillMount() {
    this.props.getCustomer(this.props.navigation.state.params.customer._id)
    const customer = this.props.navigation.state.params.customer
    this.setState({
      bodyMeasuresIndex: '',
      modalVisible: false,
      modalChartVisible: false,
      showDatePicker: false,
      bodyMeasureModalVisible: false,
      modalDeleteBodyMeasuresVisible: false,
      personData: {
        _id: customer._id,
        firstName: customer.firstName,
        lastName: customer.lastName,
        email: customer.email,
        phoneNumber: customer.phoneNumber,
        birthday: customer.birthday,
        bodyMeasure: customer.bodyMeasure
      },
      bodyMeasureData: {
        height: 0,
        weight: 0,
        shoulder: 0,
        pectorals: 0,
        arm: 0,
        belly: 0,
        hips: 0,
        thigh: 0,
        calf: 0,
      },
      errors: [],
      chartData: customer.bodyMeasure.map(measure => { return { x: `${moment(measure.measuredAt).format('DD-MM-YYYY')}`, y: parseInt(measure.height, 10) } })
    })
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.customers.actions.updateCustomer.success && nextProps.customers.actions.updateCustomer.success) {
      this._setModalVisible(false)
      this._setBodyMeasureModalVisible(false)
      this._setModalDeleteBodyMeasuresVisible(false)
      this.props.getCustomer(nextProps.customers.actions.updateCustomer.data._id)
    }

  }

  _setModalVisible(value) {
    this.setState({ modalVisible: value })
  }
  _setModalChartVisible(value) {
    this.setState({ modalChartVisible: value })
  }
  showDatePicker() {
    Keyboard.dismiss()
    this.setState({ showDatePicker: true })
  }
  _setBodyMeasureModalVisible(value) {
    this.setState({ bodyMeasureModalVisible: value })
  }
  _updateBodyCustomer() {
    this.state.personData.bodyMeasure.push(this.state.bodyMeasureData)
    this.props.updateCustomer(this.state.personData)
  }
  _setModalDeleteBodyMeasuresVisible(value) {
    this.setState({ modalDeleteBodyMeasuresVisible: value })
  }
  _deleteBodyMeasures() {
    const customer = this.props.navigation.state.params.customer
    customer.bodyMeasure.splice(this.state.bodyMeasuresIndex, 1)
    this.setState({ bodyMeasuresIndex: '' })
    this.props.updateCustomer(customer)
  }
  _chartData(data, field, color) {
    let myData = []
    data.forEach(elem => {
      if (elem[field] === null) {
        myData.push({ [field]: 0, measuredAt: `${moment(elem['measuredAt']).format('DD-MM-YYYY')}` })
      }
      else {
        myData.push({ [field]: parseInt(elem[field], 10), measuredAt: `${moment(elem['measuredAt']).format('DD-MM-YYYY')}` })
      }
    })
    return {
      serieName: field,
      data: myData.map(measure => { return { x: measure.measuredAt, y: measure[field] } }),
      color: color
    }
  }
  _validateFormRegister() {
    let res = true
    if (!utils.validateEmail(this.state.personData.email)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'this email is not valid']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.firstName)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the firstName is not valid, eg: radi ']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.lastName)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the lastName is not valid, eg: noreddine']
      }))
      res = false
    }
    if (!utils.validationPhoneNumber(this.state.personData.phoneNumber)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the phone number is not valid, ex :0483274706']
      }))
      res = false
    }
    return res
  }
  _updateCustomer() {
    this.setState({ errors: [] })
    this._validateFormRegister() ? this.props.updateCustomer(this.state.personData) : null
  }

  _updateBodyMeasureCustomer() {
    this.setState({errors: []})
    return this._validateBodyForm() ? this._updateBodyCustomer() : null
  }

  _validateBodyForm() {
    let res = true
    if (this._bodyValueEmpty()) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'impossible to register the body measurement, because the all values are empty']
      }))
      res = false
    }
    if (!this._bodyValueValid()) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'one or more value is not valid']
      }))
      res = false
    }
    return res
  }

  _bodyValueEmpty() {
    const bodyMeasure = this.state.bodyMeasureData
    return (bodyMeasure.height == '' && bodyMeasure.weight == '' &&
      bodyMeasure.shoulder == '' && bodyMeasure.pectorals == '' &&
      bodyMeasure.arm == '' && bodyMeasure.belly == '' &&
      bodyMeasure.hips == '' & bodyMeasure.thigh == '' && bodyMeasure.calf == '')
  }
  _bodyValueValid() {
    const bodyMeasure = this.state.bodyMeasureData
    return (this._verifyValueBody(bodyMeasure.height) && this._verifyValueBody(bodyMeasure.weight) && this._verifyValueBody(bodyMeasure.shoulder) &&
      this._verifyValueBody(bodyMeasure.pectorals) && this._verifyValueBody(bodyMeasure.arm) && this._verifyValueBody(bodyMeasure.belly) &&
      this._verifyValueBody(bodyMeasure.hips) && this._verifyValueBody(bodyMeasure.thigh) && this._verifyValueBody(bodyMeasure.calf))
  }
  _verifyValueBody(value) {
    return value === '' || utils.isNumber(value)
  }


  render() {
    const customer = this.props.customers.actions.getCustomer.success && this.props.customers.actions.getCustomer.data
    return (
      <Container>
        {this.props.customers.actions.getCustomer.success &&
          <Container>
            <Content>
              <Header style={styles.header} />
              <List>
                <ListItem
                  title='identity'
                  containerStyle={{ backgroundColor: '#96bad2' }}
                  rightIcon={<ElementIcon type='material-community' color={greenAdd} name='account-edit' onPress={() => this.setState({ modalVisible: !this.state.modalVisible })} />}
                />
                <ListItem
                  title='Name'
                  titleStyle={{ fontWeight: 'bold' }}
                  rightIcon={<Text note>{customer.firstName} {customer.lastName} </Text>}
                />
                <ListItem
                  title='Birthday'
                  titleStyle={{ fontWeight: 'bold' }}
                  rightIcon={<Text note>{moment(customer.birthday).utc().format('DD-MM-YYYY')} </Text>}
                />
                <ListItem
                  title='Email'
                  titleStyle={{ fontWeight: 'bold' }}
                  rightIcon={<Text note>{customer.email} </Text>}
                />
                <ListItem
                  title='body measurments'
                  containerStyle={{ backgroundColor: '#96bad2' }}
                  rightIcon={<ElementIcon type='material-community' color={greenAdd} name='plus-circle-outline' onPress={() => this._setBodyMeasureModalVisible(!this.state.bodyMeasureModalVisible)} />}
                />
              </List>
              <ScrollView>
                <List style={{}}>
                  {
                    customer.bodyMeasure.map((measure, i) => {
                      return (
                        <ListItem
                          key={i}
                          onLongPress={() => { this._setModalDeleteBodyMeasuresVisible(!this.modalDeleteBodyMeasuresVisible), this.setState({ bodyMeasuresIndex: i }) }}
                          onPress={() => this.props.navigation.navigate('BodyMeasuresDetails', { measure, customer })}
                          title={moment(measure.createdAt).format('DD/MM/YYYY')}
                          titleStyle={{ color: 'black' }}
                          leftIcon={{ type: 'material-community', name: 'ruler', color: '#4287EF' }}
                          chevronColor='green'
                        />
                      )
                    })
                  }
                </List>
              </ScrollView>
            </Content>
            {customer.bodyMeasure.length > 1 && <Fab position='bottomRight' style={{ backgroundColor: greenAdd }} containerStyle={{ right: 10, bottom: 10 }} onPress={() => this._setModalChartVisible(!this.state.modalChartVisible)}>
              <ElementIcon name='chart-areaspline' type='material-community' color='white' />
            </Fab>}
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={this.state.modalChartVisible}
              onRequestClose={() => this._setModalChartVisible(!this.state.modalChartVisible)}
            >
              <Container style={{ justifyContent: 'space-around' }}>
                <View style={{ flex: 0.5 }}>
                  <ListItem
                    title='Chart of body measurements'
                    containerStyle={styles.listItemContainer}
                    titleStyle={{
                      alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20
                    }}
                    leftIcon={<ElementIcon name='chevron-left' onPress={() => this._setModalChartVisible(!this.state.modalChartVisible)} />}
                    rightIcon={<ElementIcon name='chart-line' type='material-community' />}
                  />
                </View>
                <View style={{ flex: 2, justifyContent: 'center' }}>
                  <PureChart data={[
                    this._chartData(customer.bodyMeasure, 'pectorals', '#FF6666'),
                    this._chartData(customer.bodyMeasure, 'shoulder', '#F61919'),
                    this._chartData(customer.bodyMeasure, 'weight', '#E456DF'),
                    this._chartData(customer.bodyMeasure, 'arm', '#5F4DF9'),
                    this._chartData(customer.bodyMeasure, 'belly', '#53DFF5'),
                    this._chartData(customer.bodyMeasure, 'hips', '#41FF6D'),
                    this._chartData(customer.bodyMeasure, 'thigh', '#FFCF09'),
                    this._chartData(customer.bodyMeasure, 'calf', 'black'),
                  ]}
                    type='line' width={'95%'} height={220} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <Text style={{ backgroundColor: '#FF6666', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Pectorals</Text>
                    <Text style={{ backgroundColor: '#F61919', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Shoulder</Text>
                    <Text style={{ backgroundColor: '#E456DF', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Weight</Text>
                    <Text style={{ backgroundColor: '#5F4DF9', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Arm</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <Text style={{ backgroundColor: '#53DFF5', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Belly</Text>
                    <Text style={{ backgroundColor: '#41FF6D', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Hips</Text>
                    <Text style={{ backgroundColor: '#FFCF09', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Thigh</Text>
                    <Text style={{ backgroundColor: 'black', color: 'white', width: 70, height: 25, textAlign: 'center' }}>Calf</Text>
                  </View>

                </View>
              </Container>
            </Modal>
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => this._setModalVisible(!this.state.modalVisible)}
            >
              <Container>
                <ListItem
                  title='Edite customer'
                  containerStyle={styles.listItemContainer}
                  titleStyle={{
                    alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20,
                  }}
                  leftIcon={<ElementIcon name='chevron-left' onPress={() => { this._setModalVisible(!this.state.modalVisible), this.setState({ errors: [] }) }} />}
                  rightIcon={<ElementIcon name='save' onPress={() => { this._updateCustomer() }} />}
                />
                <Form>
                  <Item inlineLabel >
                    <Label>First Name</Label>
                    <Input value={this.state.personData.firstName}
                      onChangeText={(firstName) => this.setState({ personData: { ...this.state.personData, firstName } })} />
                  </Item>
                  <Item inlineLabel>
                    <Label>Last Name</Label>
                    <Input value={this.state.personData.lastName}
                      onChangeText={(lastName) => this.setState({ personData: { ...this.state.personData, lastName } })} />
                  </Item>
                  <Item inlineLabel>
                    <Label>Email</Label>
                    <Input value={this.state.personData.email}
                      onChangeText={(email) => this.setState({ personData: { ...this.state.personData, email } })} />
                  </Item>
                  <Item inlineLabel>
                    <Label>Phone Number</Label>
                    <Input value={this.state.personData.phoneNumber}
                      onChangeText={(phoneNumber) => this.setState({ personData: { ...this.state.personData, phoneNumber } })} />
                  </Item>
                  <Item inlineLabel last onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker })}>
                    <Label>birth date</Label>
                    <Input value={moment(this.state.personData.birthday).utc().format('YYYY/MM/DD')}
                      onChangeText={(birthday) => this.setState({ personData: { ...this.state.personData, birthday } })}
                      onFocus={() => this.showDatePicker()}
                    />
                  </Item>
                  {
                    this.state.showDatePicker &&
                    <DatePicker
                      date={this.state.personData.birthday} onDateChange={(birthday) => this.setState({ personData: { ...this.state.personData, birthday } })}
                      mode='date'
                      format='YYYY-MM-DD'
                      minDate={new Date(moment().year() - 90, 0, 1)}
                      maxDate={new Date(moment().year() - 14, 0, 1)}
                    />
                  }
                </Form>
                <Errors errors={this.state.errors}/>
              </Container>
            </Modal>
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={this.state.bodyMeasureModalVisible}
              onRequestClose={() => this._setBodyMeasureModalVisible(!this.state.bodyMeasureModalVisible)}
            >
              <Container>
                <ListItem
                  title='Add body measure'
                  titleStyle={{
                    alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20,
                  }}
                  leftIcon={<ElementIcon name='chevron-left' onPress={() => { this._setBodyMeasureModalVisible(!this.state.bodyMeasureModalVisible), this.setState({ errors: [] }) }} />}
                  rightIcon={<ElementIcon name='save' onPress={() => { this._updateBodyMeasureCustomer() }} />}
                />
                <ScrollView>
                  <Form>
                    <Item inlineLabel>
                      <Label>Height</Label>
                      <Input
                        onChangeText={(height) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, height } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Weight</Label>
                      <Input
                        onChangeText={(weight) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, weight } })} />
                      <Text note style={{ marginRight: 5 }} > kg </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Shoulder</Label>
                      <Input onChangeText={(shoulder) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, shoulder } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Pectorals</Label>
                      <Input
                        onChangeText={(pectorals) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, pectorals } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Arm</Label>
                      <Input
                        onChangeText={(arm) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, arm } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Belly</Label>
                      <Input
                        onChangeText={(belly) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, belly } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Hips</Label>
                      <Input
                        onChangeText={(hips) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, hips } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Thigh</Label>
                      <Input
                        onChangeText={(thigh) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, thigh } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                    <Item inlineLabel>
                      <Label>Calf</Label>
                      <Input
                        onChangeText={(calf) => this.setState({ bodyMeasureData: { ...this.state.bodyMeasureData, calf } })} />
                      <Text note style={{ marginRight: 5 }} > cm </Text>
                    </Item>
                  </Form>
                  <Errors errors={this.state.errors}/>
                </ScrollView>
              </Container>
            </Modal>
            <Modal
              animationType={'slide'}
              transparent={false}
              visible={this.state.modalDeleteBodyMeasuresVisible}
              onRequestClose={() => this._setModalDeleteBodyMeasuresVisible(!this.state.modalDeleteBodyMeasuresVisible)}
            >
              <Container>
                <ListItem
                  title='Delete body measure'
                  titleStyle={{ alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20, }}
                  leftIcon={<ElementIcon name='chevron-left' onPress={() => this._setModalDeleteBodyMeasuresVisible(!this.state.modalDeleteBodyMeasuresVisible)} />}
                  rightIcon={<ElementIcon name='delete' onPress={() => { this._deleteBodyMeasures() }} />}
                />
                <View style={{ margin: 10, alignItems: 'center' }}>
                  <Text>do you really want to delete this measures</Text>
                </View>
              </Container>
            </Modal>
          </Container>
        }
      </Container>
    )
  }
}


const styles = StyleSheet.create({
  header: {
    height: 1,
    backgroundColor: 'gray'
  }
})

export default Profile