


import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'
import DatePicker from 'react-native-datepicker'
import { Icon as ElementIcon, ListItem } from 'react-native-elements'
import { StyleSheet, ScrollView, FlatList, Button, Modal, DatePickerAndroid, Keyboard } from 'react-native'
import { Container, Content, Body, Text, Left, List, Right, Thumbnail, Fab, Footer, View, Header, Title, Form, Label, Input, Item, Separator, InputGroup } from 'native-base';
import * as customersActions from '../store/customers/actions'
import { greenAdd } from '../colors/colors'
import * as utils from '../utils/index'

@connect(
  state => {
    return ({
      customers: state.customers,
      currentUser: state.currentUser
    })
  },
  dispatch => bindActionCreators({ ...customersActions }, dispatch)
)

export default class Customers extends Component {

  componentWillMount() {
    this.setState({
      deleteCustomerModalVisible: false,
      customerDelete: '',
      modalVisible: false,
      showDatePicker: false,
      personData: {
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        birthday: new Date(moment().year() - 20, 0, 1),
        coach: this.props.currentUser.data._id
      },
      errors: []
    })
    this.props.getCustomers(this.props.currentUser.data._id)
  }

  _setModalVisible(val) {
    this.setState({ modalVisible: val })
  }
  _setDeletCustomerModalVisible(val) {
    this.setState({ deleteCustomerModalVisible: val })
  }
  _deleteCustomer() {
    this.props.deleteCustomer(this.state.customerDelete._id)
    this.setState({ customerDelete: '' })
  }

  showDatePicker() {
    Keyboard.dismiss()
    this.setState({ showDatePicker: true })
  }
  componentWillReceiveProps(nextProps) {
    if ((!this.props.customers.actions.createCustomer.success && nextProps.customers.actions.createCustomer.success) ||
      (!this.props.customers.actions.updateCustomer.success && nextProps.customers.actions.updateCustomer.success) ||
      (!this.props.customers.actions.deleteCustomer.success && nextProps.customers.actions.deleteCustomer.success)) {
      this._setModalVisible(false)
      this._setDeletCustomerModalVisible(false)
      this.props.getCustomers(this.props.currentUser.data._id)
    }
  }

  _validateFormRegister() {
    let res = true
    if (!utils.validateEmail(this.state.personData.email)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'this email is not valid']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.firstName)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the firstName is not valid, eg: radi ']
      }))
      res = false
    }
    if (!utils.validateName(this.state.personData.lastName)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the lastName is not valid, eg: noreddine']
      }))
      res = false
    }
    if (!utils.validationPhoneNumber(this.state.personData.phoneNumber)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the phone number is not valid, ex :0483274706']
      }))
      res = false
    }
    return res
  }
  _createCustomer() {
    this.setState({ errors: [] })
    this._validateFormRegister() ? this.props.createCustomer(this.state.personData) : null
  }
  render() {
    const { customers } = this.props
    return (
      <Container>
        <Content>
          <Header style={styles.header} />
          {(customers.success && customers.data.length === 0) && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin: 15, flexDirection:'column' }}>
            <Text>You don't have customer yet !</Text>
            <Text>Please create new customers </Text>
          </View>}
          <ScrollView>
            <List>
              {
                customers.success && customers.data.map((customer, i) => {
                  return (
                    <ListItem
                      key={customer._id}
                      onLongPress={() => { this._setDeletCustomerModalVisible(!this.state.deleteCustomerModalVisible), this.setState({ customerDelete: customer }) }}
                      onPress={() => this.props.navigation.navigate('Customer', { customer })}
                      leftIcon={<ElementIcon name='person' color='#4287EF' />}
                      title={<Text style={{ marginLeft: 10, fontWeight: 'normal' }}>{customer.firstName} {customer.lastName}</Text>}
                      chevronColor='green'
                    />
                  )
                })
              }

            </List>
          </ScrollView>
        </Content>
        <Fab position='bottomRight' style={{ backgroundColor: greenAdd }} containerStyle={{ right: 20, bottom: 10 }} onPress={() => this._setModalVisible(!this.state.modalVisible)}>
          <ElementIcon name='person-add' color='white' />
        </Fab>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this._setModalVisible(!this.state.modalVisible)}
        >
          <Container>
            <ListItem
              title='New customer'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => { this._setModalVisible(!this.state.modalVisible), this.setState({ errors: [] }) }} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._createCustomer()} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>First Name</Label>
                <Input onChangeText={(firstName) => this.setState({ personData: { ...this.state.personData, firstName } })} />
              </Item>
              <Item floatingLabel>
                <Label>Last Name</Label>
                <Input onChangeText={(lastName) => this.setState({ personData: { ...this.state.personData, lastName } })} />
              </Item>
              <Item floatingLabel>
                <Label>Email</Label>
                <Input onChangeText={(email) => this.setState({ personData: { ...this.state.personData, email } })} />
              </Item>
              <Item floatingLabel>
                <Label>Phone Number</Label>
                <Input onChangeText={(phoneNumber) => this.setState({ personData: { ...this.state.personData, phoneNumber } })} />
              </Item>
              <Item floatingLabel last onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker })}>
                <Label>birth date</Label>
                <Input value={moment(this.state.personData.birthday).format('DD/MM/YYYY')}
                  onChangeText={(birthday) => this.setState({ personData: { ...this.state.personData, birthday } })}
                  onFocus={() => this.showDatePicker()}
                />
              </Item>
              {
                this.state.showDatePicker &&
                <DatePicker
                  date={this.state.personData.birthday} onDateChange={(birthday) => this.setState({ personData: { ...this.state.personData, birthday } })}
                  mode='date'
                  format='DD/MM/YYYY'
                  minDate={new Date(moment().year() - 90, 0, 1)}
                  maxDate={new Date(moment().year() - 14, 0, 1)}
                />
              }
            </Form>
            <View style={{ flexDirection: 'column', justifyContent: 'center', margin: 10 }}>
              {this.state.errors.length > 0 && this.state.errors.map((element, i) => {
                return (
                  <Text key={i} note style={{ color: 'red', marginBottom: 5 }}>* {element}</Text>
                )
              })}
            </View>
          </Container>
        </Modal>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.deleteCustomerModalVisible}
          onRequestClose={() => this._setDeletCustomerModalVisible(!this.state.deleteCustomerModalVisible)}
        >
          <Container>
            <ListItem
              title='Delete Customer'
              titleStyle={{ alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20, }}
              leftIcon={<ElementIcon name='chevron-left' color='green' onPress={() => this._setDeletCustomerModalVisible(!this.state.deleteCustomerModalVisible)} />}
              rightIcon={<ElementIcon name='delete' onPress={() => { this._deleteCustomer() }} color='red' />}
            />
            <View style={{ margin: 10, alignItems: 'center' }}>
              <Text>do you really want to remove</Text>
              <Text> The {this.state.customerDelete.firstName} {this.state.customerDelete.lastName} customer </Text>
            </View>
          </Container>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
  },
  ListeCustomers: {
    flex: 1
  },
  header: {
    height: 1,
    backgroundColor: 'gray'
  },
  titleListItemModal: {
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemContainer: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2
  }

})
