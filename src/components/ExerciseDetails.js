
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon as ElementIcon, ListItem, Button, Overlay } from 'react-native-elements'
import moment from 'moment'
import { StyleSheet, ImageBackground, ScrollView, FlatList, Modal, Image, DatePickerAndroid, Keyboard } from 'react-native'
import { Container, Content, Body, Badge, Text, Left, List, Right, Thumbnail, Fab, Footer, View, Header, Title, Form, Label, Input, Item, Card, CardItem, Separator, InputGroup } from 'native-base'
import * as exercisesActions from '../store/exercises/actions'
import { road, smokesky, greenAdd, golf, } from '../colors/colors'
import * as utils from '../utils/index'
import Errors from './errors'

@connect(
  state => {
    return ({
      exercises: state.exercises
    })
  },
  dispatch => bindActionCreators({ ...exercisesActions }, dispatch)
)

class ExerciseDetails extends React.Component {

  componentWillMount() {
    this.props.getExercise(this.props.navigation.state.params.exercise._id)
    this.setState({
      modalVisible: false,
      deleteSetModalVisible: false,
      updateSetModalVisible: false,
      setIndex: '',
      exercise: this.props.exercises.actions.getExercise.data,
      setData: {
        weight: '',
        nbrRepetition: ''
      },
      errors: []
    })
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.exercises.actions.updateExercise.success && nextProps.exercises.actions.updateExercise.success) {
      this._setModalVisible(false)
      this._setDeleteSetModalVisible(false)
      this._setUpdateSetModalVisible(false)
      this.setState({ exercise: nextProps.exercises.actions.updateExercise.data })
    }
    if (!this.props.exercises.actions.getExercise.success && nextProps.exercises.actions.getExercise.success) {
      this.setState({ exercise: nextProps.exercises.actions.getExercise.data })
    }

  }
  _setModalVisible(value) {
    this.setState({ modalVisible: value })
  }
  _updateExercise() {
    this.state.exercise.sets.push(this.state.setData)
    this.props.updateExercise(this.state.exercise)
  }
  _updateSet(set, index) {
    this.state.exercise.sets.splice(index, 1, set)
    this.props.updateExercise(this.state.exercise)
  }
  _setDeleteSetModalVisible(value) {
    this.setState({ deleteSetModalVisible: value })
  }
  _setUpdateSetModalVisible(value) {
    this.setState({ updateSetModalVisible: value })
  }
  _deleteSet(index) {
    this.state.exercise.sets.splice(index, 1)
    this.props.updateExercise(this.state.exercise)
  }
  _setData(data) {
    this.setState(prevState => ({
      setData: {
        ...prevState.setData,
        weight: `${data.weight}`,
        nbrRepetition: `${data.nbrRepetition}`
      }
    }))
  }

  _validateForm() {
    let res = true
    if (!utils.validateWeight(this.state.setData.weight)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the weight must be between 1 and 150 kg !, eg:65 ']
      }))
      res = false
    }
    if (!utils.validateNbrRepetition(this.state.setData.nbrRepetition)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the number of repetition is not valid !, eg: 10']
      }))
      res = false
    }
    return res
  }
  _preUpdateExercise() {
    this.setState({errors: []})
    return this._validateForm() ? this._updateExercise() : null
  }
  _preUpdateSet(set, index) {
    this.setState({errors: []})
    return this._validateForm() ? this._updateSet(set, index) : null
  }

  render() {
    const { exercises } = this.props
    return (
      <Container style={{ flex: 1 }}>
        {exercises.actions.getExercise.success && <View style={{ flex: 1, paddingTop: 5, backgroundColor: greenAdd }}>
          <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', }}>
            <View style={{ alignItems: 'center' }}>
              <Text style={{ color: road, fontSize: 20 }}>{this.state.exercise.title}</Text>
              <Text style={{ fontStyle: 'italic', color: road }}>{moment(this.state.exercise.createdAt).format('DD MMM YY', 'fr')}</Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <ElementIcon type='material-community' name='ticket' size={60} color='white' />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 3 }}>
              <View>
                <Text style={{ color: road }}>{this.state.exercise.sets.length} Sets</Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-end', }}>
                <Text style={{ color: road }}>Recovery Time</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <ElementIcon type='material-community' name='timer' size={20} />
                  <Text style={{ color: road, fontStyle: 'italic' }}>{this.state.exercise.recoveryTime} sec</Text>
                </View>
              </View>
              <View>
                <Text style={{ color: road }}>{this.state.exercise.sets.reduce((res, set) => res + set['nbrRepetition'], 0)} Reps</Text>
              </View>
            </View>
          </View>
        </View>}
        <View style={{ flex: 2 }} >
        {(exercises.actions.getExercise.success && exercises.actions.getExercise.data.sets.length === 0) && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin: 15, flexDirection:'column' }}>
            <Text>You don't have set yet !</Text>
            <Text>Please create new sets </Text>
          </View>}
          <ScrollView>
            <List>
              {
                (exercises.actions.getExercise.success && this.state.exercise.sets.length > 0) && this.state.exercise.sets.map((set, i) => {
                  return (
                    <View key={i} style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 1, padding:5, borderBottomColor:'#E0E5E2' }} >
                      <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={{ color: road }}>Weight</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <Text>{set.weight}</Text>
                          <ElementIcon type='material-community' name='weight-kilogram' />
                        </View>
                      </View>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: road }}>{set.nbrRepetition > 0 && `${set.nbrRepetition} Reps`}</Text>
                      </View >
                      <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                        <ElementIcon type='material-community' name='pencil' color='#10ADFF' onPress={() => { this._setUpdateSetModalVisible(!this.state.updateSetModalVisible), this.setState({ setIndex: i }), this._setData(set) }} />
                        <ElementIcon type='material-community' name='delete' color='red' onPress={() => { this._setDeleteSetModalVisible(!this.state.deleteSetModalVisible), this.setState({ setIndex: i }) }} />
                      </View>
                    </View>
                  )
                })
              }
            </List>
          </ScrollView>
        </View>
        <Fab position='bottomRight' style={{ backgroundColor: greenAdd }} containerStyle={{ right: 25, bottom: 10 }} onPress={() => this._setModalVisible(!this.state.modalVisible)}>
          <ElementIcon name='add' color='white' />
        </Fab>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this._setModalVisible(!this.state.modalVisible)}
        >
          <Container>
            <ListItem
              title='New set'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setModalVisible(!this.state.modalVisible), this.setState({error: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._preUpdateExercise()} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>Weight</Label>
                <Input onChangeText={(weight) => this.setState({ setData: { ...this.state.setData, weight } })} />
              </Item>
              <Item floatingLabel>
                <Label>Repetition number</Label>
                <Input onChangeText={(nbrRepetition) => this.setState({ setData: { ...this.state.setData, nbrRepetition } })} />
              </Item>
            </Form>
            <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
        <Modal
          animationType={'none'}
          transparent={false}
          visible={this.state.deleteSetModalVisible}
          onRequestClose={() => this._setDeleteSetModalVisible(!this.state.deleteSetModalVisible)}
        >
          <Container>
            <ListItem
              title='Delete set'
              titleStyle={{ alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20, }}
              hideChevron
            />
            <View style={{ margin: 10, alignItems: 'center' }}>
              <Text>Do you really want to remove</Text>
              <Text> The Set ?</Text>
            </View>
            <View style={{ margin: 20, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
              <Button title='Cancel' buttonStyle={{ backgroundColor: "#66FCB1", width: 80, borderRadius: 3 }} onPress={() => this._setDeleteSetModalVisible(!this.state.deleteSetModalVisible)} />
              <Button title='Delete' buttonStyle={{ backgroundColor: "#FC4E7D", width: 80, borderRadius: 3 }} onPress={() => { this._deleteSet(this.state.setIndex) }} />
            </View>
          </Container>
        </Modal>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.updateSetModalVisible}
          onRequestClose={() => this._setUpdateSetModalVisible(!this.state.updateSetModalVisible)}
        >
          <Container>
            <ListItem
              title='update set'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setUpdateSetModalVisible(!this.state.updateSetModalVisible),this.setState({errors: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._preUpdateSet(this.state.setData, this.state.setIndex)} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>Weight</Label>
                <Input value={this.state.setData.weight} onChangeText={(weight) => this.setState({ setData: { ...this.state.setData, weight } })} />
              </Item>
              <Item floatingLabel>
                <Label>Repetition number</Label>
                <Input value={this.state.setData.nbrRepetition} onChangeText={(nbrRepetition) => this.setState({ setData: { ...this.state.setData, nbrRepetition } })} />
              </Item>
            </Form>
            <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
      </Container>
    )
  }
}


const styles = StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
  },
  ListeCustomers: {
    flex: 1
  },
  header: {
    height: 1,
    backgroundColor: 'gray'
  },
  titleListItemModal: {
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemContainer: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2
  },
  buttonStyle: {
    width: 150,
    backgroundColor: 'red'
  }
})
export default ExerciseDetails