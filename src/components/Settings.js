import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon as ElementIcon, ListItem } from 'react-native-elements'
import { ScrollView } from 'react-native'
import { Container, Header, InputGroup, Input, Button, Icon, List, Content, Text } from 'native-base'
import currentUserActions from '../store/currentUser/actions'

@connect(
  state => {
    return ({
      currentUser: state.currentUser
    })
  },
  dispatch => bindActionCreators({ ...currentUserActions }, dispatch)
)

export default class Settings extends Component {

  componentWillMount() {

  }


  render() {
    return (
      <Container>
        <ListItem
          rightIcon={<ElementIcon name='logout' type='material-community' color='green'/>}
          title='logOut'
          titleStyle={{marginHorizontal: 20, fontSize: 20, fontWeight: '500'}}
          onPress={()=>{this.props.logOut()}}
        />
      </Container>
    )
  }
}