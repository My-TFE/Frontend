import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Container, Text, Form, Label, Input, Item } from 'native-base'



class Errors extends React.Component {


  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', margin: 15 }}>
        {this.props.errors.length > 0 && this.props.errors.map((element, i) => {
          return (
            <Text key={i} note style={{ color: 'red', marginBottom: 5 }}> * {element}</Text>
          )
        })}
      </View>
    )
  }
}

const styles = StyleSheet.create({})

export default Errors