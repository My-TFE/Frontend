import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Icon as ElementIcon, ListItem, Button } from 'react-native-elements'
import moment from 'moment'
import { StyleSheet, ScrollView, FlatList,  Modal, DatePickerAndroid, Keyboard } from 'react-native'
import { Container, Content, Body, Text, Left, List, Right, Thumbnail, Fab, Footer, View, Header, Title, Form, Label, Input, Item, Separator, InputGroup } from 'native-base'
import * as sessionsActions from '../store/sessions/actions'
import { greenAdd } from '../colors/colors'
import * as utils from '../utils';
import Errors from './errors'


@connect(
  state => {
    return ({
      sessions: state.sessions,
      exercises: state.exercises
    })
  },
  dispatch => bindActionCreators({ ...sessionsActions }, dispatch)
)
class Session extends React.Component {



  componentWillMount() {
    const customerId = this.props.navigation.state.params.customerId
    this.setState({
      modalVisible: false,
      deleteSessionModalVisible: false,
      updateSessionModalVisible: false,
      sessionDelete: '',
      sessionData: {
        title: '',
        customer: customerId,
      },
      errors: []
    })
    this.props.getSessions(customerId)
  }
  componentWillReceiveProps(nextProps) {
    if (!this.props.sessions.actions.createSession.success && nextProps.sessions.actions.createSession.success) {
      this._setModalVisible(false)
      this.props.getSessions(this.props.navigation.state.params.customerId)
    }
    if (!this.props.sessions.actions.updateSession.success && nextProps.sessions.actions.updateSession.success) {
      this.props.getSessions(this.props.navigation.state.params.customerId)
      this._setUpdateSessionModalVisible(false)
    }
    if (!this.props.sessions.actions.deleteSession.success && nextProps.sessions.actions.deleteSession.success) {
      this._setDeleteSessionModalVisible(false)
      this.props.getSessions(this.props.navigation.state.params.customerId)
    }
    if (!this.props.exercises.actions.updateExercise.success && nextProps.exercises.actions.updateExercise.success) {
      this.props.getSessions(this.props.navigation.state.params.customerId)
    }
  }
  _setModalVisible(value) {
    this.setState({ modalVisible: value })
  }
  _setDeleteSessionModalVisible(value) {
    this.setState({ deleteSessionModalVisible: value })
  }
  _setUpdateSessionModalVisible(value) {
    this.setState({updateSessionModalVisible: value})
  }
  _deleteSession() {
    this.props.deleteSession(this.state.sessionDelete._id)
    this.setState({ sessionDelete: '' })
  }
  _setSession(session) {
    this.setState(prevState => ({
      sessionData: {
        ...prevState.sessionData,
        _id:  `${session._id}`,
        title: `${session.title}`,
      }
    }))
  }
  _titleValidate() {
    let res = true
    if (!utils.validateTitle(this.state.sessionData.title)) {
      this.setState(prevState => ({
        errors: [...prevState.errors, 'the title is not valid ! eg: session 1']
      }))
      res = false
    }
    return res
  }
  _createSession() {
    this.setState({errors: []})
    return this._titleValidate() ? this.props.createSession(this.state.sessionData) : null
  }
  _updateSession() {
    this.setState({errors: []})
    return this._titleValidate() ? this.props.updateSession(this.state.sessionData) : null
  }

  render() {
    const { sessions } = this.props
    return (
      <Container>
        <Content>
          <Header style={styles.header} />
          {(sessions.success && sessions.data.length === 0) && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin: 15, flexDirection:'column' }}>
            <Text>You don't have session yet !</Text>
            <Text>Please create new sessions </Text>
          </View>}
          <ScrollView>
            <List>
              {
                sessions.success && sessions.data.map((session, i) => {
                  return (
                    <View key={i} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', margin: 10, borderBottomWidth: 1, padding:5, borderBottomColor:'#E0E5E2' }}>
                      <View style={{ flex: 2, flexDirection: 'column' }}>
                        <Text>{session.title}</Text>
                        <Text note>{session.exercises.length} Exercices</Text>
                      </View>
                      <View style={{ flex: 2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <ElementIcon type='material-community' name='pencil' color='#10ADFF' onPress={() => {this._setUpdateSessionModalVisible(!this.state.updateSessionModalVisible), this._setSession(session) }} />
                        <ElementIcon type='material-community' name='delete' color='red' onPress={() => { this._setDeleteSessionModalVisible(!this.state.deleteSessionModalVisible), this.setState({ sessionDelete: session }) }} />
                      </View>
                      <View style={{ flex: 1.65, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text note>{moment(session.createdAt).format('DD/MM/YYYY')}</Text>
                        <ElementIcon type='material-community' name='chevron-right' color='green' onPress={() => this.props.navigation.navigate('Exercises', { session })} />
                      </View>
                    </View>
                  )
                })
              }
            </List>
          </ScrollView>
        </Content>
        <Fab position='bottomRight' style={{ backgroundColor: greenAdd }} containerStyle={{ right: 10, bottom: 10 }} onPress={() => this._setModalVisible(!this.state.modalVisible)}>
          <ElementIcon name='add' color='white' />
        </Fab>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this._setModalVisible(!this.state.modalVisible)}
        >
          <Container>
            <ListItem
              title='New Session'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setModalVisible(!this.state.modalVisible), this.setState({errors: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._createSession()} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>Title</Label>
                <Input onChangeText={(title) => this.setState({ sessionData: { ...this.state.sessionData, title } })} />
              </Item>
            </Form>
            <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.deleteSessionModalVisible}
          onRequestClose={() => this._setDeleteSessionModalVisible(!this.state.deleteSessionModalVisible)}
        >
          <Container>
            <ListItem
              title='Delete Session'
              titleStyle={{ alignItems: 'center', textAlign: 'center', fontWeight: 'bold', fontSize: 20, }}
              hideChevron
            />
            <View style={{ margin: 10, alignItems: 'center' }}>
              <Text>Do you really want to remove</Text>
              <Text> The session {this.state.sessionDelete.title} ?</Text>
            </View>
            <View style={{ margin: 20, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
              <Button title='Cancel' buttonStyle={{ backgroundColor: "#66FCB1", width: 80, borderRadius: 3 }} onPress={() => this._setDeleteSessionModalVisible(!this.state.deleteSessionModalVisible)} />
              <Button title='Delete' buttonStyle={{ backgroundColor: "#FC4E7D", width: 80, borderRadius: 3 }} onPress={() => { this._deleteSession() }} />
            </View>
          </Container>
        </Modal>
        <Modal
          animationType={'none'}
          transparent={false}
          visible={this.state.updateSessionModalVisible}
          onRequestClose={() => this._setUpdateSessionModalVisible(!this.state.updateSessionModalVisible)}
        >
          <Container>
            <ListItem
              title='update Session'
              containerStyle={styles.listItemContainer}
              titleStyle={styles.titleListItemModal}
              leftIcon={<ElementIcon name='chevron-left' onPress={() => {this._setUpdateSessionModalVisible(!this.state.updateSessionModalVisible), this.setState({errors: []})}} />}
              rightIcon={<ElementIcon name='save' onPress={() => this._updateSession(this.state.sessionData)} />}
            />
            <Form>
              <Item floatingLabel>
                <Label>Title</Label>
                <Input value={this.state.sessionData.title} onChangeText={(title) => this.setState({ sessionData: { ...this.state.sessionData, title } })} />
              </Item>
            </Form>
            <Errors errors={this.state.errors}/>
          </Container>
        </Modal>
      </Container>
    )

  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
  },
  ListeCustomers: {
    flex: 1
  },
  header: {
    height: 1,
    backgroundColor: 'gray'
  },
  titleListItemModal: {
    alignItems: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemContainer: {
    borderBottomColor: 'gray',
    borderBottomWidth: 2
  }
})

export default Session