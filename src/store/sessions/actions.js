import { SessionsService } from './services'
import promiseActionCreator from '../_helpers/promiseActionCreator'
import { GET_SESSIONS, CREATE_SESSION, DELETE_SESSION, UPDATE_SESSION } from './constants'

export function getSessions(customerId) {
	return promiseActionCreator(SessionsService.getSessions(customerId), GET_SESSIONS)
}
export function createSession(data) {
	return promiseActionCreator(SessionsService.createSession(data), CREATE_SESSION)
}
export function deleteSession(sessionId) {
	return promiseActionCreator(SessionsService.deleteSession(sessionId), DELETE_SESSION)
}
export function updateSession(session) {
	return promiseActionCreator(SessionsService.updateSession(session), UPDATE_SESSION)
}


