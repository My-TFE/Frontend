import { GET_SESSIONS, CREATE_SESSION, DELETE_SESSION, UPDATE_SESSION } from './constants'
import { initialState } from './initialState'

const requestAction = (state) => {
  return {
    ...state,
    loading: true,
    success: false,
    error: ''
  }
}
const faildAction = (state, error) => {
  return {
    ...state,
    loading: false,
    success: false,
    error: error
  }
}
export function sessions(state = initialState, action) {

  switch (action.type) {

    case `${GET_SESSIONS}_REQUEST`:
      return requestAction(state)

    case `${GET_SESSIONS}_SUCCESS`:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        success: true,
        error: ''
      }

    case `${GET_SESSIONS}_FAILED`:
      return faildAction(state, action.error)

    case `${CREATE_SESSION}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createSession: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${CREATE_SESSION}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createSession: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${CREATE_SESSION}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createSession: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    case `${DELETE_SESSION}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteSession: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${DELETE_SESSION}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteSession: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${DELETE_SESSION}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteSession: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

      case `${UPDATE_SESSION}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateSession: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${UPDATE_SESSION}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateSession: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${UPDATE_SESSION}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateSession: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    default:
      return state
  }
}