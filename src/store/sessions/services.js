import API from '../_common/services/api'

export const SessionsService = {

	getSessions(customerId) {
		return API.get(`/sessions/${customerId}`)
	},
	createSession(data) {
		return API.post('/sessions', data)
	},
	deleteSession(sessionId) {
		return API.delete(`/sessions/${sessionId}`)
	},
	updateSession(session) {
		return API.put(`/sessions/${session._id}`, session)
	}

}
