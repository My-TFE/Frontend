import { GET_CUSTOMERS, CREATE_CUSTOMER, UPDATE_CUSTOMER, GET_CUSTOMER, DELETE_CUSTOMER } from './constants'
import { initialState } from './initialState'

const requestAction = (state) => {
  return {
    ...state,
    loading: true,
    success: false,
    error: ''
  }
}
const faildAction = (state, error) => {
  return {
    ...state,
    loading: false,
    success: false,
    error: error
  }
}
export function customers(state = initialState, action) {

  switch (action.type) {

    case `${GET_CUSTOMERS}_REQUEST`:
      return requestAction(state)

    case `${GET_CUSTOMERS}_SUCCESS`:
      return {
        ...state,
        data: action.payload.data,
        loading: false,
        success: true,
        error: ''
      }

    case `${GET_CUSTOMERS}_FAILED`:
      return faildAction(state, action.error)

    case `${CREATE_CUSTOMER}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createCustomer: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${CREATE_CUSTOMER}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createCustomer: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${CREATE_CUSTOMER}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          createCustomer: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    case `${UPDATE_CUSTOMER}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateCustomer: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${UPDATE_CUSTOMER}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateCustomer: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${UPDATE_CUSTOMER}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          updateCustomer: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    case `${GET_CUSTOMER}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          getCustomer: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${GET_CUSTOMER}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          getCustomer: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${GET_CUSTOMER}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          getCustomer: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    case `${DELETE_CUSTOMER}_REQUEST`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteCustomer: {
            loading: true,
            success: false,
            error: ''
          }
        }
      }

    case `${DELETE_CUSTOMER}_SUCCESS`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteCustomer: {
            loading: false,
            success: true,
            data: action.payload.data,
            error: ''
          }
        }
      }

    case `${DELETE_CUSTOMER}_FAILED`:
      return {
        ...state,
        actions: {
          ...state.actions,
          deleteCustomer: {
            loading: false,
            success: true,
            error: action.error
          }
        }
      }

    default:
      return state
  }
}