import {CustomersService} from './services'
import promiseActionCreator from '../_helpers/promiseActionCreator'
import {GET_CUSTOMERS, CREATE_CUSTOMER, UPDATE_CUSTOMER, GET_CUSTOMER, DELETE_CUSTOMER} from './constants'

export function getCustomers(coachId) {
	 return promiseActionCreator(CustomersService.getCustomers(coachId), GET_CUSTOMERS)
}
export function createCustomer(data) {
	 return promiseActionCreator(CustomersService.createCustomer(data), CREATE_CUSTOMER)
}
export function updateCustomer(data) {
	 return promiseActionCreator(CustomersService.updateCustomer(data), UPDATE_CUSTOMER)
}
export function getCustomer(customerId) {
	 return promiseActionCreator(CustomersService.getCustomer(customerId), GET_CUSTOMER)
}
export function deleteCustomer(customerId) {
	 return promiseActionCreator(CustomersService.deleteCustomer(customerId), DELETE_CUSTOMER)
}

