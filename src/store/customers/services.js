import API from '../_common/services/api'
import { updateCustomer } from './actions';


export const CustomersService = {

	getCustomers(coachId) {
		return API.get(`/coach/${coachId}/customers`)
	},
	createCustomer(customerdata) {
		return API.post('/users', customerdata)
	},
	updateCustomer(customerData) {
		return API.put(`/users/${customerData._id}`, customerData)
	},
	getCustomer(customerId) {
		return API.get(`/users/${customerId}`)
	}, 
	deleteCustomer(customerId) {
		return API.delete(`/users/${customerId}`)
	}

}
