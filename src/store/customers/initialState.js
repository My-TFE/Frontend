import { updateCustomer } from "./actions";

export const initialState = {
    data: [],
    actions: {
        createCustomer: {},
        updateCustomer: {},
        getCustomer: {},
        deleteCustomer: {}
    }
}