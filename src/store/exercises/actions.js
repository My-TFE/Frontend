import {ExercisesService} from './services'
import promiseActionCreator from '../_helpers/promiseActionCreator'
import {GET_EXERCISES, CREATE_EXERCISE, UPDATE_EXERCISE, DELETE_EXERCISE, GET_EXERCISE} from './constants'

export function getExercises(sessionId) {
	 return promiseActionCreator(ExercisesService.getExercises(sessionId), GET_EXERCISES)
}
export function createExercise(data) {
	 return promiseActionCreator(ExercisesService.createExercise(data), CREATE_EXERCISE)
}
export function updateExercise(exercise) {
	return promiseActionCreator(ExercisesService.updateExercise(exercise), UPDATE_EXERCISE)
}
export function deleteExercise(exerciseId) {
	return promiseActionCreator(ExercisesService.deleteExercise(exerciseId), DELETE_EXERCISE)
}
export function getExercise(exerciseId) {
	return promiseActionCreator(ExercisesService.getExercise(exerciseId), GET_EXERCISE)
}


