
export const initialState = {
    data: [],
    actions: {
        createExercise: {},
        updateExercise: {},
        deleteExercise: {},
        getExercise: {}
    }
}