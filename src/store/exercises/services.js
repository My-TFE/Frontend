import API from '../_common/services/api'
import { deleteExercise } from './actions';

export const ExercisesService = {

	getExercises(sessionId) {
		return API.get(`/exercises/all/${sessionId}`)
	},
	createExercise(data) {
		return API.post('/exercises', data)
	},
	updateExercise(exercise) {
		return API.put(`/exercises/${exercise._id}`, exercise)
	},
	deleteExercise(exerciseId) {
		return API.delete(`/exercises/${exerciseId}`)
	},
	getExercise(exerciseId) {
		return API.get(`/exercises/${exerciseId}`)
	}
}
