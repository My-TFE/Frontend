import {GET_EXERCISES, CREATE_EXERCISE, UPDATE_EXERCISE, DELETE_EXERCISE, GET_EXERCISE} from './constants'
import {initialState} from './initialState'

const requestAction = (state) => {
  return {
    ...state,
    loading: true,
    success: false,
    error: ''
  }
}
const faildAction = (state, error) => {
  return  {
    ...state,
    loading: false,
    success: false,
    error: error
  }
}
export function exercises(state = initialState, action) {

	switch (action.type) {

		case `${GET_EXERCISES}_REQUEST`:
			return requestAction(state)

		case `${GET_EXERCISES}_SUCCESS`:
			return {
				...state,
				data: action.payload.data,
				loading: false,
				success: true,
				error: ''
			}

		case `${GET_EXERCISES}_FAILED`:
			return faildAction(state, action.error)
		
		case `${CREATE_EXERCISE}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					createExercise: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${CREATE_EXERCISE}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					createExercise: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
      }
      
		case `${CREATE_EXERCISE}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					createExercise: {
						loading: false,
						success: true,
						error: action.error
					}
				}
      }
      
      case `${UPDATE_EXERCISE}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					updateExercise: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${UPDATE_EXERCISE}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					updateExercise: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
			}

		case `${UPDATE_EXERCISE}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					updateExercise: {
						loading: false,
						success: true,
						error: action.error
					}
				}
      }
      
      case `${DELETE_EXERCISE}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					deleteExercise: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${DELETE_EXERCISE}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					deleteExercise: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
			}

		case `${DELETE_EXERCISE}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					deleteExercise: {
						loading: false,
						success: true,
						error: action.error
					}
				}
      }

      case `${GET_EXERCISE}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					getExercise: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${GET_EXERCISE}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					getExercise: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
			}

		case `${GET_EXERCISE}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					getExercise: {
						loading: false,
						success: true,
						error: action.error
					}
				}
      }
      
		default:
			return state
	}
}