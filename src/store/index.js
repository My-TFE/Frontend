import {createStore, applyMiddleware, combineReducers} from 'redux'
import thunk from 'redux-thunk'
import {customers} from './customers/reducers' 
import {sessions} from './sessions/reducers'
import {exercises} from './exercises/reducers'
import {currentUser} from './currentUser/reducers'

function getRootReducer() {
	return combineReducers({
		customers, 
		sessions, 
		exercises,
		currentUser

	})
}

export default createStore(
		getRootReducer(),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
		applyMiddleware(thunk)
	) 

