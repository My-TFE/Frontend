import {currentUserService} from './services'
import promiseActionCreator from '../_helpers/promiseActionCreator'
import {GET_CURRENT_USER, SIGN_IN_USER, SIGN_UP_USER} from './constants'

const actions = {
  getSignedInUser() {
    return promiseActionCreator(currentUserService.getSignedInUser(), GET_CURRENT_USER)
  },
  signInWithEmail(credentials) {
    return promiseActionCreator(currentUserService.signInWithEmail(credentials), SIGN_IN_USER)
  },
  logOut() {
    return promiseActionCreator(currentUserService.logOut(), GET_CURRENT_USER)
  },
  signup(coach) {
    return promiseActionCreator(currentUserService.signup(coach), SIGN_UP_USER)
  }
}

export default actions
