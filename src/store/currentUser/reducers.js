import {SIGN_IN_USER,GET_CURRENT_USER, SIGN_UP_USER} from './constants'
import {initialState} from './initialState'

const requestAction = (state) => {
  return {
    ...state,
    loading: true,
    success: false,
    error: ''
  }
}
const faildAction = (state, error) => {
  return  {
    ...state,
    loading: false,
    success: false,
    error: error
  }
}
export function currentUser(state = initialState, action) {

	switch (action.type) {

		case `${GET_CURRENT_USER}_REQUEST`:
			return requestAction(state)

		case `${GET_CURRENT_USER}_SUCCESS`:
			return {
				...state,
				data: action.payload.data,
				loading: false,
				success: true,
				error: ''
			}

		case `${GET_CURRENT_USER}_FAILED`:
			return faildAction(state, action.error)
		
		case `${SIGN_IN_USER}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					signInWithEmail: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${SIGN_IN_USER}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					signInWithEmail: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
			}

		case `${SIGN_IN_USER}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					signInWithEmail: {
						loading: false,
						success: true,
						error: action.error
					}
				}
      }
      
      case `${SIGN_UP_USER}_REQUEST`:
			return {
				...state,
				actions: {
					...state.actions,
					signup: {
						loading: true,
						success: false,
						error:''
					}
				}
			}
		
		case `${SIGN_UP_USER}_SUCCESS`:
			return {
				...state,
				actions: {
					...state.actions,
					signup: {
						loading: false,
						success: true,
						data: action.payload.data,
						error:''
					}
				}
			}

		case `${SIGN_UP_USER}_FAILED`:
			return {
				...state,
				actions: {
					...state.actions,
					signup: {
						loading: false,
						success: false,
						error: action.error
					}
				}
      }
      
		default:
			return state
	}
}