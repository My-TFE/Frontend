import API from '../_common/services/api'
import { AsyncStorage } from "react-native"

export const currentUserService = {
  getSignedInUser() {
     return AsyncStorage.getItem('token')
      .then(token => {
        if (token) {
          API.setToken(token)
        }
      })
      .then(_ => { return this.getCurrentUser()})
      .catch(err => console.log(err))
   
  },
  getCurrentUser() {
    return API.get(`/auth/me`)
  },
  signInWithEmail(credentials) {
    console.log('connexion noreddine')
    return API.post('/auth/login', credentials)
      .then(token => {
        this.setUserToken(token.data)
        return token
      })
  },
  signup(coachData) {
    return API.post('/coach', coachData)
   /*  .then(coach => {
      return this.signInWithEmail({email:coach.data.email, password:coach.data.password})
    })  */
  },
  setUserToken(token) {
    AsyncStorage.setItem('token', token)
      .then(_ => {})
      .catch(err => console.log(err))
    AsyncStorage.getItem('token')
    .then(_ => {})
    return API.setToken(token)

  },
  logOut() {
    API.setToken(null)
    AsyncStorage.removeItem('token')
    .then(_ => {})
    .catch(err => console.log(err))
    return this.getCurrentUser()
  }

}
