import React from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import {Provider} from 'react-redux'
import { StyleSheet, Text, View } from 'react-native'
import {Container} from "native-base"
import Login from './src/components/Login'
import TabNavigation from './src/router/TabNavigation'
import Store from './src/store/index'
import currentUserActions from './src/store/currentUser/actions'

@connect(
  state => {
    return ({
      currentUser: state.currentUser
    })
  }, 
  dispatch => bindActionCreators({ ...currentUserActions }, dispatch)
)

class AppWithNavigationState extends React.Component {

  componentWillMount() {
    this.props.getSignedInUser()
  }

	render() {
    const {currentUser} = this.props
		return (
			<Container style={styles.container}>
				{!currentUser.success && <Login />}
				{currentUser.success && <TabNavigation />}
			</Container>
		)
	}
}

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true }
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
        <Provider store={Store}>
          <AppWithNavigationState />
			</Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20
  },
});
